import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-dashboard-doctor',
  templateUrl: './dashboard-doctor.component.html',
  styleUrls: ['./dashboard-doctor.component.css']
})
export class DashboardDoctorComponent implements OnInit {

  datos:any;

  constructor(private servicio:IniciarSesionService, private router:Router) { }

  ngOnInit(): void {
    this.traerDatosDoctor();
  }

  traerDatosDoctor(){
    let correo = localStorage.getItem("userMyC")+"";

    this.servicio.listarUsuariosEmail(correo).subscribe(data=>{
      this.datos = data;
    });
  }


  cerrarSesion(){
    localStorage.removeItem("userMyC");
    this.router.navigate(['/']);
  }


}
