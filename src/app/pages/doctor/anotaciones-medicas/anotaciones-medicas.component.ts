import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-anotaciones-medicas',
  templateUrl: './anotaciones-medicas.component.html',
  styleUrls: ['./anotaciones-medicas.component.css']
})
export class AnotacionesMedicasComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {

    //this.ventanaSecundaria("http://localhost:4200/dashboard-doctor/ins-enfermera"); 

  }
  
  hojaPaciente(){
    this.router.navigate(['/dashboard-doctor/hoja-anotaciones']);
  }
  visualizar(){
    this.ventanaSecundaria("http://localhost:4200/consulta-instrucciones"); 
    
  }
  ventanaSecundaria (URL:string){ 
    window.open(URL,"ventana1","width=800,height=600,scrollbars=NO") 
 } 
 
 
}
