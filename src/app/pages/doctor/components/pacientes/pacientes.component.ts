import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { CirugiaModel } from 'src/app/models/doctor/cirugia.model';
import { Diagnostico } from 'src/app/models/doctor/diagnostico.model';
import { CirujiasService } from 'src/app/services/doctor/cirujias.service';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';
import { PacientesServicesService } from 'src/app/services/pacientes/pacientes-services.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {

  datos:any;
  datosPaciente:any;
  d:string = '';
  formulario:FormGroup;
  cedula:string = "";
  constructor(private servicio:CirujiasService, private fb:FormBuilder, private servicioLogin:IniciarSesionService,private servicioPacientes:PacientesServicesService) {
    this.formulario = this.fb.group({
      diagnostico:['',Validators.required]
    });
   }

  ngOnInit(): void {
    this.listarPacientes();
  }

 editar(id:any){

   let d:Diagnostico ={
     diagnostico:this.formulario.get("diagnostico")?.value
   } 
   this.servicio.editarDiagnostico(id,d).subscribe(data=>{
      alert("Exito!")
   });
 }
 
  listarPacientes(){
    let email = ""+localStorage.getItem("userMyC");
    this.servicioLogin.listarUsuariosEmail(email).subscribe(data=>{
      this.datos = data;
     // console.log(this.datos)
      this.cedula = this.datos[0].numero_cedula;

      this.servicioPacientes.listarPacientesCedula(this.cedula).subscribe(data=>{
        this.datosPaciente = data;
      //  console.log(this.datosPaciente);
      });

    });
    
    
  }
        
}
