import { Component, OnInit } from '@angular/core';
import { CirujiasService } from 'src/app/services/doctor/cirujias.service';

@Component({
  selector: 'app-prog-cirugias',
  templateUrl: './prog-cirugias.component.html',
  styleUrls: ['./prog-cirugias.component.css']
})
export class ProgCirugiasComponent implements OnInit {

  constructor(private servicio:CirujiasService) { }
  listarCirugias:any;

  ngOnInit(): void {
    this.consultarCirugias();
  }
  consultarCirugias(){
    this.servicio.listarCirujias().subscribe( data =>{
      this.listarCirugias = data;
    });
  }

}
