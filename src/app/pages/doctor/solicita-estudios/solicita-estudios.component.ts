import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EstudioQuimicoModel } from 'src/app/models/quimicos/estudios-quimicos.model';
import { DoctoresService } from 'src/app/services/doctor/doctores.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-solicita-estudios',
  templateUrl: './solicita-estudios.component.html',
  styleUrls: ['./solicita-estudios.component.css']
})
export class SolicitaEstudiosComponent implements OnInit {

  personalForm: FormGroup;
  constructor(private servicioDoctor: DoctoresService, private fb: FormBuilder) {

    this.personalForm = this.fb.group({
      nombre_paciente: ['', Validators.required],
      numero_habitacion: ['', Validators.required],
      estudio_solicitado: ['', Validators.required],
      fecha: ['', Validators.required],
      hora: ['', Validators.required],
      nombre_doctor: ['', Validators.required],
      nivel_prioridad: ['', Validators.required]
    });

  }



  ngOnInit(): void {
  }

  registroEstudioPacienteDoctor() {

    let datos: EstudioQuimicoModel = {
      nombre_paciente: this.personalForm.get("nombre_paciente")?.value,
      numero_habitacion: this.personalForm.get("numero_habitacion")?.value,
      estudio_solicitado: this.personalForm.get("estudio_solicitado")?.value,
      fecha: this.personalForm.get("fecha")?.value,
      hora: this.personalForm.get("hora")?.value,
      nombre_doctor: this.personalForm.get("nombre_doctor")?.value,
      nivel_prioridad: this.personalForm.get("nivel_prioridad")?.value
    }

    this.servicioDoctor.registrarEstudiosQuimicosDoctorPacientes(datos).subscribe( data=>{
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Cita registrada correctamente',
        showConfirmButton: true
      });
      window.location.reload();
    } );
  }
}
