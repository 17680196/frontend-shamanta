import { Component, OnInit } from '@angular/core';
import { CirujiasService } from 'src/app/services/doctor/cirujias.service';

@Component({
  selector: 'app-consultar-cirugia',
  templateUrl: './consultar-cirugia.component.html',
  styleUrls: ['./consultar-cirugia.component.css']
})
export class ConsultarCirugiaComponent implements OnInit {

  variable: any;

  constructor(private servicio:CirujiasService) { }



  ngOnInit(): void {
    this.consultar();
  }


  consultar(){
    this.servicio.listarCirujias().subscribe(data => {
        this.variable=data;
        console.log(this.variable[0]);

    });

  }

}
