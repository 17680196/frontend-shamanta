import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-trabajador-social',
  templateUrl: './trabajador-social.component.html',
  styleUrls: ['./trabajador-social.component.css']
})
export class TrabajadorSocialComponent implements OnInit {

  usuario:any;
  constructor(private servicio:IniciarSesionService, private router:Router) { }

  ngOnInit(): void {

    this.traerDatos();

  }
  traerDatos(){
    let email = localStorage.getItem("userMyC")+"";
    this.servicio.listarUsuariosEmail(email).subscribe(data=>{
      this.usuario = data;
      localStorage.setItem("rol",this.usuario[0].rol);
    });
  }
  cerrarSesion(){
    localStorage.removeItem("userMyC");
    this.router.navigate(['/login']);
  }

}
