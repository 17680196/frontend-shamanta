import { Component, OnInit } from '@angular/core';
import { CitasMedicas } from 'src/app/models/trabajo-social/citas-medicas.model';
import { RegistrarCitaService } from 'src/app/services/trabajo-social/registrar-cita.service';


@Component({
  selector: 'app-consultar-citas',
  templateUrl: './consultar-citas.component.html',
  styleUrls: ['./consultar-citas.component.css']
})
export class ConsultarCitasComponent implements OnInit {

  Citas:any;
  
  constructor(private servicio:RegistrarCitaService) {
   
  }

  ngOnInit(): void {

    this.traerCitas();
  
  }
  traerCitas(){
    this.servicio.listarCitas().subscribe((data:CitasMedicas)=>{
      this.Citas = data;
      console.log(this.Citas);
    });
  }

}
