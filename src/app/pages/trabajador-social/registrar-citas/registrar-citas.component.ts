import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CitasMedicas } from 'src/app/models/trabajo-social/citas-medicas.model';
import { RegistrarCitaService } from 'src/app/services/trabajo-social/registrar-cita.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-citas',
  templateUrl: './registrar-citas.component.html',
  styleUrls: ['./registrar-citas.component.css']
})
export class RegistrarCitasComponent implements OnInit {

  datos:any;
  personalForm:FormGroup;
  

  constructor(private servicio:RegistrarCitaService, private fb:FormBuilder) { 
    
    this.personalForm = this.fb.group({
      nombre_solicitante:['', Validators.required],
      fecha:['',Validators.required],
      hora:['', Validators.required],
      asunto:['', Validators.required],
      nombre_doctor:['', Validators.required],
      numero_consultorio:['', Validators.required]
    });
  
  }

  ngOnInit(): void {
    this.listarDoctores()
  }

  listarDoctores(){
    let rol = "Doctor";
    this.servicio.listarDoctores(rol).subscribe(data=>{
      this.datos = data;
      console.log(data);
    });
  }

  
  registarCitas(){
    let DatosFormulario:CitasMedicas = {
      nombre_solicitante: this.personalForm.get("nombre_solicitante")?.value,
      fecha: this.personalForm.get("fecha")?.value,
      hora:this.personalForm.get("hora")?.value,
      asunto:this.personalForm.get("asunto")?.value,
      nombre_doctor:this.personalForm.get("nombre_doctor")?.value,
      numero_consultorio: this.personalForm.get("numero_consultorio")?.value
    }
    this.servicio.registrarCita(DatosFormulario).subscribe(data=>{
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Cita registrada correctamente',
        showConfirmButton: true,
        /* timer: 1500 */
      });
      window.location.reload();
    });
  }

}
