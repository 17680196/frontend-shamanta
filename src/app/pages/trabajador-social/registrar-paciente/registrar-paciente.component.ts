import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PacientesModel } from 'src/app/models/trabajo-social/pacientes.model';
import { DoctoresService } from 'src/app/services/doctor/doctores.service';
import { PacientesServicesService } from 'src/app/services/pacientes/pacientes-services.service';
import { PacientesService } from 'src/app/services/trabajo-social/pacientes.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-registrar-paciente',
  templateUrl: './registrar-paciente.component.html',
  styleUrls: ['./registrar-paciente.component.css']
})
export class RegistrarPacienteComponent implements OnInit {

  personalForm:FormGroup;
  listaDoctores: any;
  listaEnfermera: any;
  constructor(private fb:FormBuilder, private servicio:PacientesService,private servicioPacientes:PacientesServicesService ,private servicioDoctores:DoctoresService) {

    this.personalForm = this.fb.group({
      idCama:['', Validators.required],
      nombre:['', Validators.required],
      apellido_paterno:['', Validators.required],
      apellido_materno:['', Validators.required],
      peso:['', Validators.required],
      edad:['',Validators.required],
      diagnostico:['', Validators.required],
      tipoSangre:['', Validators.required],
      doctorEncargado:['', Validators.required],
      enfermeraCargo:['', Validators.required]
    });

   }

  ngOnInit(): void {
    this.OnDoctores();
    this.OnEnfermeros();
  }

  OnDoctores(){
    this.servicioDoctores.listarDoctoresRol("Doctor").subscribe( data =>{
      this.listaDoctores = data;
    });
  }

  OnEnfermeros(){
    this.servicioDoctores.listarDoctoresRol("Enfermero").subscribe( data =>{
      this.listaEnfermera = data;
    });
  }

  registrarPaciente(){
    let modeloPaciente:PacientesModel = {
      idCama:this.personalForm.get("idCama")?.value,
      nombre:this.personalForm.get("nombre")?.value,
      apellido_paterno:this.personalForm.get("apellido_paterno")?.value,
      apellido_materno:this.personalForm.get("apellido_materno")?.value,
      peso:this.personalForm.get("peso")?.value,
      edad:this.personalForm.get("edad")?.value,
      diagnostico:this.personalForm.get("diagnostico")?.value,
      tipoSangre:this.personalForm.get("tipoSangre")?.value,
      doctorEncargado:this.personalForm.get("doctorEncargado")?.value,
      enfermeraCargo:this.personalForm.get("enfermeraCargo")?.value
    }
    
    this.servicioPacientes.registrarPaciente(modeloPaciente).subscribe( (resp)=>{
      Swal.fire({
        icon:'success',
        position:'center',
        title:'Registro exitoso'
      });
      this.personalForm.reset();
    }); 
    
  }
}
