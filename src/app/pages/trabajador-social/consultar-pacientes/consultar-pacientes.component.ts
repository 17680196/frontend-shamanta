import { Component, OnInit } from '@angular/core';
import { PacientesServicesService } from 'src/app/services/pacientes/pacientes-services.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-consultar-pacientes',
  templateUrl: './consultar-pacientes.component.html',
  styleUrls: ['./consultar-pacientes.component.css']
})
export class ConsultarPacientesComponent implements OnInit {
  rol = localStorage.getItem("rol");
  lista:any;
  constructor( private servicio: PacientesServicesService ) { }

  ngOnInit(): void {
    this.TraerPacientesGeneral();
  }
  TraerPacientesGeneral(){
    //if( this.rol == "Trabajador Social" ){
      this.servicio.listarPacienteGeneral().subscribe(data=>{
        this.lista = data;
        
      });
    //}
  }
  eliminarPaciente(id:string){

    Swal.fire({
      title: '¿Esta seguro de eliminar el registro?',
      text: "Una vez borrado no podra recuperarlo!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar!'
    }).then((result) => {
      if (result.isConfirmed) {


        this.servicio.eliminarPaciente(id).subscribe(data=>{
          Swal.fire({
            icon:'success',
            position:'center',
            title:'Registro eliminado con exito'
          });
          window.location.reload();
        });
        
      }
    })

    
  }

}
