import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuariosModel } from 'src/app/models/login/registrar-usuarios.model';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-enfermera',
  templateUrl: './registrar-enfermera.component.html',
  styleUrls: ['./registrar-enfermera.component.css']
})
export class RegistrarEnfermeraComponent implements OnInit {

  personalForm:FormGroup;
  constructor(private servicio:IniciarSesionService, private fb:FormBuilder) {

    this.personalForm = this.fb.group({
      email:['', Validators.required],
      password:['', Validators.required],
      nombre:['', Validators.required],
      apellido_paterno:['', Validators.required],
      apellido_materno:['', Validators.required],
      numero_cedula:['', Validators.required],
      celular:['', Validators.required],
      sexo:['', Validators.required]
    });

   }

  ngOnInit(): void {
  }

  registrarEnfermera(){

    let modeloDoctor:UsuariosModel = {
      email:this.personalForm.get("email")?.value,
      password:this.personalForm.get("password")?.value,
      nombre:this.personalForm.get("nombre")?.value,
      apellido_paterno:this.personalForm.get("apellido_paterno")?.value,
      apellido_materno:this.personalForm.get("apellido_materno")?.value,
      rol:"Enfermero",
      numero_cedula:this.personalForm.get("numero_cedula")?.value,
      celular:this.personalForm.get("celular")?.value,
      sexo:this.personalForm.get("sexo")?.value
    }

    this.servicio.registrarUsuarios(modeloDoctor).subscribe(data=>{
      Swal.fire({
        icon:'success',
        position:'center',
        title:'Registro exitoso'
      });
      this.personalForm.reset();
    });

  }
}
