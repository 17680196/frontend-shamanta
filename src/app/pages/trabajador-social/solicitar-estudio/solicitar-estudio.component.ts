import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EstudioQuimicoModel } from 'src/app/models/quimicos/estudios-quimicos.model';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';
import { PacientesServicesService } from 'src/app/services/pacientes/pacientes-services.service';
import { EstudioQuimicoService } from 'src/app/services/quimicos/estudio-quimico.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-solicitar-estudio',
  templateUrl: './solicitar-estudio.component.html',
  styleUrls: ['./solicitar-estudio.component.css']
})
export class SolicitarEstudioComponent implements OnInit {
  
  datos:any;
  misDatos:any;

  personalForm:FormGroup;
  
  constructor(private fb:FormBuilder,private servicio:PacientesServicesService, private _servicio:IniciarSesionService,private servicioEstudio:EstudioQuimicoService) { 
    this.personalForm = this.fb.group({
        nombre_paciente:['',Validators.required],
        numero_habitacion:['',Validators.required],
        estudio_solicitado:['',Validators.required],
        fecha:['',Validators.required],
        hora:['',Validators.required],
        nombre_doctor:['',Validators.required],
        nivel_prioridad:['',Validators.required]
    });
  }

  ngOnInit(): void {
    this.traerDatos();
  }
  traerDatos(){

    let email = localStorage.getItem("userMyC")+"";
    
    this.servicio.listarPacientes().subscribe(data=>{
      this.datos = data;
      
    this._servicio.listarUsuariosEmail(email).subscribe(Misdatos=>{
      this.misDatos = Misdatos;
      this.personalForm.get("nombre_doctor")?.setValue(this.misDatos[0].nombre+" "+this.misDatos[0].apellido_paterno+" "+this.misDatos[0].apellido_materno);
    });
    });
  }

  registrarEstudio(){
    let modelo : EstudioQuimicoModel = {
      nombre_paciente:this.personalForm.get("nombre_paciente")?.value,
      numero_habitacion:this.personalForm.get("numero_habitaciones")?.value,
      estudio_solicitado:this.personalForm.get("estudio_solicitado")?.value,
      fecha:this.personalForm.get("fecha")?.value,
      hora:this.personalForm.get("hora")?.value,
      nombre_doctor:this.personalForm.get("nombre_doctor")?.value,
      nivel_prioridad:this.personalForm.get("nivel_prioridad")?.value
      
    };

    console.log(modelo);
   this.servicioEstudio.registrarSolicitudEstudio(modelo).subscribe(data=>{
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Su registro satisfactorio',
      showConfirmButton: false,
      timer: 1500
    })
   });

   this.personalForm.reset();

  }

}
