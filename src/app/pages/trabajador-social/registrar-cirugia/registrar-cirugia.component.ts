import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CirujiasService } from 'src/app/services/doctor/cirujias.service';
import Swal from 'sweetalert2';
import { CirugiaModel } from './../../../models/doctor/cirugia.model'

@Component({
  selector: 'app-registrar-cirugia',
  templateUrl: './registrar-cirugia.component.html',
  styleUrls: ['./registrar-cirugia.component.css']
})
export class RegistrarCirugiaComponent implements OnInit {

  formulario:FormGroup;
  listarCirugias:any = [];

  constructor(private servicio:CirujiasService, private fb:FormBuilder) {
    this.formulario = this.fb.group({
      idConsultorio:['', Validators.required],
      nombre:['', Validators.required],
      tipo_cirugia: ['', Validators.required],
      fecha: ['', Validators.required],
      hora: ['', Validators.required],
      nombre_doctor: ['', Validators.required],
    });
   }

  ngOnInit(): void {
    
  }

  registrarCirugia(){
    
      let modelo:CirugiaModel = {
        idConsultorio: this.formulario.get("idConsultorio")?.value,
        nombre: this.formulario.get("nombre")?.value,
        tipo_cirugia: this.formulario.get("tipo_cirugia")?.value,
        fecha: this.formulario.get("fecha")?.value,
        hora: this.formulario.get("hora")?.value,
        nombre_doctor: this.formulario.get("nombre_doctor")?.value,
      }
      console.log(modelo)
    this.servicio.registrarCirujias(modelo).subscribe(data=>{
          
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: '¡Cirugia registrada exitosamente!',
            showConfirmButton: false,
            timer: 1500
          })
        }
      ); 
    
  }
}
