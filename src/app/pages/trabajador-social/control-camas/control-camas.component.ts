import { Component, OnInit } from '@angular/core';
import { ControlCamasService } from 'src/app/services/trabajo-social/control-camas.service';

@Component({
  selector: 'app-control-camas',
  templateUrl: './control-camas.component.html',
  styleUrls: ['./control-camas.component.css']
})
export class ControlCamasComponent implements OnInit {

  camas:any;
  constructor(private servicio_camas:ControlCamasService) { }

  ngOnInit(): void {
    this.traerCamas();
  }

  traerCamas(){
    this.servicio_camas.listarCamas().subscribe(data=>{
      this.camas = data;
    });
  }

}
