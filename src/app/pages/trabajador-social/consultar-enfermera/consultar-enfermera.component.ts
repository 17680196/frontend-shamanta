import { Component, OnInit } from '@angular/core';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-consultar-enfermera',
  templateUrl: './consultar-enfermera.component.html',
  styleUrls: ['./consultar-enfermera.component.css']
})
export class ConsultarEnfermeraComponent implements OnInit {
  datos:any;
  filtrado = '';
  constructor(private servicio:IniciarSesionService) { }

  ngOnInit(): void {
    this.traerEnfermeros();
  }

  traerEnfermeros(){
    this.servicio.listarEnfermeros().subscribe(data=>{
      this.datos = data;
    });
  }
  eliminarDoctor(id:any){
    Swal.fire({
      title: '¿Estas seguro?',
      text: "Al borrar un registro no podrás recuperarlo",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar.',
      cancelButtonText:'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.servicio.borrarUsuarios(id).subscribe(data=>{
          Swal.fire(
            'Registro eliminado',
            'Tu registro fue eliminado satisfactoriamente',
            'success'
          )
          window.location.reload();
        });

        
      }
    })
  }

  buscar(texto:string){
    this.filtrado = texto;
  }
  

}
