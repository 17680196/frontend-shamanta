import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-dashboard-quimico',
  templateUrl: './dashboard-quimico.component.html',
  styleUrls: ['./dashboard-quimico.component.css']
})
export class DashboardQuimicoComponent implements OnInit {

  datos:any;

  constructor(private servicio:IniciarSesionService, private router:Router) { }

  ngOnInit(): void {
    this.traerDatosPrincipales();
  }
  traerDatosPrincipales(){
    let email=localStorage.getItem("userMyC")+"";
    this.servicio.listarUsuariosEmail(email).subscribe(data=>{
      this.datos = data;
    });
  }

  cerrarSesion(){
    localStorage.removeItem("userMyC");
    this.router.navigate(['/dashboard-quimico']);
  }

}
