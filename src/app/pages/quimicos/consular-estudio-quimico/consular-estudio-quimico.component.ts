import { Component, OnInit } from '@angular/core';
import { EstudioQuimicoService } from 'src/app/services/quimicos/estudio-quimico.service';

@Component({
  selector: 'app-consular-estudio-quimico',
  templateUrl: './consular-estudio-quimico.component.html',
  styleUrls: ['./consular-estudio-quimico.component.css']
})
export class ConsularEstudioQuimicoComponent implements OnInit {

  listaDatos:any

  constructor(private servicio:EstudioQuimicoService) { }

  ngOnInit(): void {
    this.listarSolicitudes();
  }

  listarSolicitudes(){
    this.servicio.listarEstudios().subscribe( data => {
        this.listaDatos = data;
        console.log(this.listaDatos)
     })
  }

}
