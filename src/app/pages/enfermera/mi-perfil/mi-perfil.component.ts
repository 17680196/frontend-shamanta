import { Component, OnInit } from '@angular/core';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-mi-perfil',
  templateUrl: './mi-perfil.component.html',
  styleUrls: ['./mi-perfil.component.css']
})
export class MiPerfilComponent implements OnInit {

  datos:any;
  constructor(private servicio:IniciarSesionService) { }

  ngOnInit(): void {
    this.traerDatos();
  }
  traerDatos(){
    let email = ""+localStorage.getItem("userMyC");
    this.servicio.listarUsuariosEmail(email).subscribe(data=>{
      this.datos = data;
    });
  }

}
