import { Component, OnInit } from '@angular/core';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-dashboard-enfermera',
  templateUrl: './dashboard-enfermera.component.html',
  styleUrls: ['./dashboard-enfermera.component.css']
})
export class DashboardEnfermeraComponent implements OnInit {

  datosEnfermero:any;

  constructor(private servicio:IniciarSesionService) { }

  ngOnInit(): void {
    this.traerDatosRol();
  }

  traerDatosRol(){
    let email = localStorage.getItem("userMyC")+"";
    this.servicio.listarUsuariosEmail(email).subscribe(data=>{
      this.datosEnfermero = data;
      console.log(this.datosEnfermero);
    });
  }

  cerrarSesion(){
    localStorage.removeItem("userMyC");
  }
}
