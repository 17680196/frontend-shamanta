import { Component, OnInit } from '@angular/core';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  datosEnfermero:any;
  constructor(private servicio:IniciarSesionService) { }

  ngOnInit(): void {
    this.traerDatosRol();
  }

  traerDatosRol(){
    let email = localStorage.getItem("userMyC")+"";
    this.servicio.listarUsuariosEmail(email).subscribe(data=>{
      this.datosEnfermero = data;
      console.log(this.datosEnfermero);
    });
  }

}
