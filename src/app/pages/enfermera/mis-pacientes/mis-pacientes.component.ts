import { Component, OnInit } from '@angular/core';
import { EnfermeraServicesService } from 'src/app/services/enfermera/enfermera-services.service';

@Component({
  selector: 'app-mis-pacientes',
  templateUrl: './mis-pacientes.component.html',
  styleUrls: ['./mis-pacientes.component.css']
})
export class MisPacientesComponent implements OnInit {

  listaEnfermera:any;

  constructor( private servicio: EnfermeraServicesService ) { }

  ngOnInit(): void {
    this.listarPacientes();
  }

  listarPacientes(){
    this.servicio.listarPacienteEnfermera("Enfermero").subscribe( data=> {
      this.listaEnfermera = data;
      console.log(this.listaEnfermera);
    });
  }

}
