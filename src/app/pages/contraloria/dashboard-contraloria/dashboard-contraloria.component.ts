import { Component, OnInit } from '@angular/core';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-dashboard-contraloria',
  templateUrl: './dashboard-contraloria.component.html',
  styleUrls: ['./dashboard-contraloria.component.css']
})
export class DashboardContraloriaComponent implements OnInit {

  datos:any;
  constructor(private servicio:IniciarSesionService) { }

  ngOnInit(): void {
    this.traerDatos();
  }

  traerDatos(){
    let email = localStorage.getItem("userMyC")+"";
    this.servicio.listarUsuariosEmail(email).subscribe(data=>{
      this.datos = data;
    });
  }

}
