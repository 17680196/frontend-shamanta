import { Component, OnInit } from '@angular/core';
import { RegistrarCitaService } from 'src/app/services/trabajo-social/registrar-cita.service';

@Component({
  selector: 'app-cobro-consultas',
  templateUrl: './cobro-consultas.component.html',
  styleUrls: ['./cobro-consultas.component.css']
})
export class CobroConsultasComponent implements OnInit {

  citas:any;
  precioPago:number = 0;

  constructor(private servicio:RegistrarCitaService) { }

  ngOnInit(): void {
    this.traerCitasMedicas();
  }
  traerCitasMedicas(){
    this.servicio.listarCitas().subscribe(data=>{
      this.citas = data;
      this.logicaPrecios();
    });



  }

  logicaPrecios(){
    if(this.citas[0].asunto == "Certificado medico"){
      this.precioPago = 100;
    }
    if(this.citas[0].asunto == "Control del niño sano"){
      this.precioPago = 500;
    }
    if(this.citas[0].asunto == "Control de embarazo"){
      this.precioPago = 300;
    }
    if(this.citas[0].asunto == "Planificacion_familiar"){
      this.precioPago = 400;
    }
    if("Oximetria"){
      this.precioPago = 100;
    }
    if("Toma de presion arterial"){
      this.precioPago = 150;
    }
    if("Aplicacion de inyeccion"){
      this.precioPago = 100;
    }
    if("Prueba para detectar niveles de azúcar"){
      this.precioPago = 150;
    }
    if("Lavado Ótico"){
      this.precioPago = 200;
    }
    if("Retiro de puntos"){
      this.precioPago = 300;
    }
    if("Retiro de sondas"){
      this.precioPago = 150;
    }
    if("Extraccion de uña enterrada"){
      this.precioPago = 150;
    }
    if(this.citas[0].asunto == "Nebulizaciones"){
      this.precioPago = 250;
    }
    if(this.citas[0].asunto == "Curacion"){
      this.precioPago = 300;
    }
    if(this.citas[0].asunto == "Sutura"){
      this.precioPago = 150;
    }
    if(this.citas[0].asunto == "Cita con el dentista."){
      this.precioPago = 500;
    }
  }
}
