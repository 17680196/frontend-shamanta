import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IniciarSesion } from 'src/app/models/login/iniciar-sesion.model';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-dashboard-interconsultas',
  templateUrl: './dashboard-interconsultas.component.html',
  styleUrls: ['./dashboard-interconsultas.component.css']
})
export class DashboardInterconsultasComponent implements OnInit {
datos:any;
  constructor(private servicio:IniciarSesionService,private router:Router) { }

  ngOnInit(): void {
    this.traerDatos();
  }
traerDatos(){
      this.servicio.listarUsuariosEmail(localStorage.getItem('userMyC')+"").subscribe(data=>{
        this.datos=data;
      }        )
}
cerrarSesion(){
  localStorage.removeItem('userMyC');
  this.router.navigate(['/'])
}
}
