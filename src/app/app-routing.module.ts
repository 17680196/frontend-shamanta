import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {LoginComponent} from './auth/login/login.component';
import { NopagesfoundComponent } from './pages/nopagesfound/nopagesfound.component';

/* Dashboard Doctor */

import { DashboardEnfermeraComponent } from './pages/enfermera/dashboard-enfermera.component';
import { MisPacientesComponent } from './pages/enfermera/mis-pacientes/mis-pacientes.component';
import { BanosPacientesComponent } from './pages/enfermera/banos-pacientes/banos-pacientes.component';
import { CirugiasComponent } from './pages/enfermera/cirugias/cirugias.component';
import { MaterialAsignadoComponent } from './pages/enfermera/material-asignado/material-asignado.component';
import { SolicitarAseoComponent } from './pages/enfermera/solicitar-aseo/solicitar-aseo.component';
import { TrabajadorSocialComponent } from './pages/trabajador-social/trabajador-social.component';
import { NuevoBanoComponent } from './pages/enfermera/nuevo-bano/nuevo-bano.component';
import { InicioComponent } from './pages/enfermera/inicio/inicio.component';
import { MiPerfilComponent } from './pages/enfermera/mi-perfil/mi-perfil.component';
import { ControlCamasComponent } from './pages/trabajador-social/control-camas/control-camas.component';
import { RegistrarCitasComponent } from './pages/trabajador-social/registrar-citas/registrar-citas.component';
import { ConsultarCitasComponent } from './pages/trabajador-social/consultar-citas/consultar-citas.component';
import { SolicitarEstudioComponent } from './pages/trabajador-social/solicitar-estudio/solicitar-estudio.component';
import { ConsultarEstudiosComponent } from './pages/trabajador-social/consultar-estudios/consultar-estudios.component';

/* Dashboard Doctor */
import { DashboardDoctorComponent } from './pages/doctor/dashboard-doctor/dashboard-doctor.component';
import { PacientesComponent } from './pages/doctor/components/pacientes/pacientes.component';
import { AnotacionesMedicasComponent } from './pages/doctor/anotaciones-medicas/anotaciones-medicas.component';
import { InsEnfermeraComponent } from './pages/doctor/ins-enfermera/ins-enfermera.component';
import { ProgCirugiasComponent } from './pages/doctor/prog-cirugias/prog-cirugias.component';
import { SolicitaEstudiosComponent } from './pages/doctor/solicita-estudios/solicita-estudios.component';
import { HojaAnotacionesComponent } from './pages/doctor/anotaciones-medicas/hoja-anotaciones/hoja-anotaciones.component';
import { DashboardQuimicoComponent } from './pages/quimicos/dashboard-quimico/dashboard-quimico.component';
import { ConsularEstudioQuimicoComponent } from './pages/quimicos/consular-estudio-quimico/consular-estudio-quimico.component';
import { PruebasPatologicasComponent } from './pages/quimicos/pruebas-patologicas/pruebas-patologicas.component';
import { DashboardContraloriaComponent } from './pages/contraloria/dashboard-contraloria/dashboard-contraloria.component';

import { RegistrarProvComponent } from './pages/contraloria/registrar-prov/registrar-prov.component';
import { ConsultarProvComponent } from './pages/contraloria/consultar-prov/consultar-prov.component';
import { CobroConsultasComponent } from './pages/contraloria/cobro-consultas/cobro-consultas.component';
import { CobroCirugiasComponent } from './pages/contraloria/cobro-cirugias/cobro-cirugias.component';
import { ControlGastosComponent } from './pages/contraloria/control-gastos/control-gastos.component';
import { DashboardInterconsultasComponent } from './pages/jefe-interconsultas/dashboard-interconsultas/dashboard-interconsultas.component';



import { RegistrarDoctorComponent } from './pages/trabajador-social/registrar-doctor/registrar-doctor.component';
import { ConsultarDoctoresComponent } from './pages/trabajador-social/consultar-doctores/consultar-doctores.component';
import { RegistrarEnfermeraComponent } from './pages/trabajador-social/registrar-enfermera/registrar-enfermera.component';
import { ConsultarEnfermeraComponent } from './pages/trabajador-social/consultar-enfermera/consultar-enfermera.component';
import { RegistrarPacienteComponent } from './pages/trabajador-social/registrar-paciente/registrar-paciente.component';
import { RegistrarGastosComponent } from './pages/contraloria/registrar-gastos/registrar-gastos.component';
import { RegistrarCirugiaComponent } from './pages/trabajador-social/registrar-cirugia/registrar-cirugia.component';
import { ConsultarCirugiaComponent } from './pages/trabajador-social/consultar-cirugia/consultar-cirugia.component';

import { ConsultaPruebaComponent } from './pages/quimicos/consulta-prueba/consulta-prueba.component';

import { ConsultarPacientesComponent } from './pages/trabajador-social/consultar-pacientes/consultar-pacientes.component';
import { AsesoriaHospitalariaComponent } from './pages/jefe-interconsultas/asesoria-hospitalaria/asesoria-hospitalaria.component';
import { InstruccionesDoctorComponent } from './pages/doctor/instrucciones-doctor/instrucciones-doctor.component';








const routes: Routes = [
  {path:'', component:LoginComponent},
  { path:'login', component: LoginComponent },
  { path:'nopagefound', component: NopagesfoundComponent},
  
  

  { 
    path:'dashboard-enfermera', 
    component:DashboardEnfermeraComponent, 
    children: [
      {path:'', component: InicioComponent},
      {path:'mi-perfil', component:MiPerfilComponent},
      {path:'mis-pacientes', component:MisPacientesComponent},
      {path:'banos-pacientes',component:BanosPacientesComponent},
      {path:'cirugias',component:CirugiasComponent},
      {path:'material-asignado',component:MaterialAsignadoComponent},
      {path: 'solicitar-aseo',component:SolicitarAseoComponent},
      {path: 'nuevo-baño', component:NuevoBanoComponent,}
    ] 
  },

  {
    path: 'trabajador-social',
    component: TrabajadorSocialComponent,
    children:[
      {path:'control-camas', component:ControlCamasComponent},
      {path:'mi-perfil', component:MiPerfilComponent},
      {path:'registrar-citas', component:RegistrarCitasComponent},
      {path:'consultar-citas', component:ConsultarCitasComponent},
      {path:'solicitar-estudio', component:SolicitarEstudioComponent},
      {path:'consultar-estudios', component:ConsultarEstudiosComponent},
      {path: 'registrar-doctor', component: RegistrarDoctorComponent},
      {path: 'consultar-doctores', component: ConsultarDoctoresComponent},
      {path: 'registrar-enfermera', component: RegistrarEnfermeraComponent},
      {path: 'consultar-enfermera', component: ConsultarEnfermeraComponent},
      {path:'registrar-paciente', component: RegistrarPacienteComponent},
      {path: 'registrar-cirugia', component:RegistrarCirugiaComponent},
      {path: 'consultar-cirugia', component:ConsultarCirugiaComponent},
      {path:'consultar-pacientes', component:ConsultarPacientesComponent},

    ]


  },
  { 
    path:'dashboard-doctor', 
    component:DashboardDoctorComponent, 
    children:[
        { path:'pacientes', component:PacientesComponent},
        {path:'mi-perfil', component:MiPerfilComponent},
        { path: 'anotaciones-medicas', component:AnotacionesMedicasComponent},
        { path:'hoja-anotaciones', component: HojaAnotacionesComponent},
        {path: 'ins-enfermera', component:InsEnfermeraComponent},
        {path: 'prog-cirugias', component:ProgCirugiasComponent},
        {path: 'registrar-cirugia', component:RegistrarCirugiaComponent},
        {path: 'solicita-estudios', component:SolicitaEstudiosComponent},  
    ] 
  },
  {
    path:'consulta-instrucciones',
    component:InstruccionesDoctorComponent
  },

  {
    path:'dashboard-quimico',
    component:DashboardQuimicoComponent,
    children:[
      {path:'mi-perfil', component:MiPerfilComponent},
      {path:"visualizar-estudios",component:ConsularEstudioQuimicoComponent},
      {path:"pruebas-patologica",component:PruebasPatologicasComponent},
      {path:"consulta-prueba",component:ConsultaPruebaComponent},

    ] 
  },

  {
    path:"dashboard-contraloria",
    component:DashboardContraloriaComponent,
    children:[
      {path:'mi-perfil', component:MiPerfilComponent},
      {path:'registrar-prov',component:RegistrarProvComponent},
      {path:'consultar-prov',component:ConsultarProvComponent},
      {path:'cobro-consultas',component:CobroConsultasComponent},
      {path:'cobro-cirugias',component:CobroCirugiasComponent},
      {path:'control-gastos',component:ControlGastosComponent},
      {path:'registrar-gastos',component:RegistrarGastosComponent},
    ]

  },

{
  path:'dashboard-interconsultas',
  component:DashboardInterconsultasComponent,
  children:[
    {path:'asesoria-hospitalaria', component:AsesoriaHospitalariaComponent},
    {path:'seguimiento-pacientes', component:AsesoriaHospitalariaComponent},
  ]
},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
