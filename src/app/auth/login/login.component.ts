import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IniciarSesion } from 'src/app/models/login/iniciar-sesion.model';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  personalForm:FormGroup;
  constructor( private fb:FormBuilder, private servicio:IniciarSesionService,private router:Router ) {
    this.personalForm = this.fb.group({
      email:['',Validators.required],
      password:['', Validators.required]
    });
   }

  ngOnInit(): void {
  }

  iniciarSesion(){
    let modelo:IniciarSesion = {
      email:this.personalForm.get("email")?.value,
      password:this.personalForm.get("password")?.value
    }
    localStorage.setItem("userMyC", modelo.email)
    this.servicio.login(modelo).subscribe(data=>{
      console.log(data);

      if(data == "Enfermera"){
        this.router.navigate(['/dashboard-enfermera']);
      }
      if(data == "Enfermero"){
        this.router.navigate(['/dashboard-enfermera']);
      }
      if(data == "Trabajador Social"){
        this.router.navigate(['/trabajador-social']);
      }
      if(data == "Doctor"){
        this.router.navigate(['/dashboard-doctor']);
      }
      if(data == "Quimico"){
        this.router.navigate(['/dashboard-quimico']);
      }
      if(data == "Contraloria"){
        this.router.navigate(['/dashboard-contraloria']);
      }
      if(data == "Jefe Interconsultas"){
        this.router.navigate(['/dashboard-interconsultas']);
      }

      //Faltan los otros roles

    });

  }
}
