import { Pipe, PipeTransform } from '@angular/core';
import { UsuariosModel } from '../models/login/registrar-usuarios.model';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(usuarios:UsuariosModel[], search:string = "" ): any {
    
    let resultados = [];

    for( let post of usuarios ){
      if((post.email.indexOf(search) > -1
      || post.password.indexOf(search) > -1
      || post.nombre.indexOf(search) > -1
      || post.apellido_paterno.indexOf(search) > -1
      || post.apellido_materno.indexOf(search) > -1
      || post.numero_cedula.indexOf(search) > -1
      || post.celular.indexOf(search) > -1
      || post.sexo.indexOf(search) > -1
      || post.rol.indexOf(search) > -1))
      {
        resultados.push(post);
      }
    }

    return resultados;

  }

}
