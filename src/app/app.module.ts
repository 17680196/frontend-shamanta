import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NopagesfoundComponent } from './pages/nopagesfound/nopagesfound.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardDoctorComponent } from './pages/doctor/dashboard-doctor/dashboard-doctor.component';
import { DashboardEnfermeraComponent } from './pages/enfermera/dashboard-enfermera.component';
import { MisPacientesComponent } from './pages/enfermera/mis-pacientes/mis-pacientes.component';
import { BanosPacientesComponent } from './pages/enfermera/banos-pacientes/banos-pacientes.component';
import { CirugiasComponent } from './pages/enfermera/cirugias/cirugias.component';
import { MaterialAsignadoComponent } from './pages/enfermera/material-asignado/material-asignado.component';
import { SolicitarAseoComponent } from './pages/enfermera/solicitar-aseo/solicitar-aseo.component';
import { TrabajadorSocialComponent } from './pages/trabajador-social/trabajador-social.component';
import { NuevoBanoComponent } from './pages/enfermera/nuevo-bano/nuevo-bano.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InicioComponent } from './pages/enfermera/inicio/inicio.component';
import { MiPerfilComponent } from './pages/enfermera/mi-perfil/mi-perfil.component';
import { ControlCamasComponent } from './pages/trabajador-social/control-camas/control-camas.component';
import { RegistrarCitasComponent } from './pages/trabajador-social/registrar-citas/registrar-citas.component';
import { ConsultarCitasComponent } from './pages/trabajador-social/consultar-citas/consultar-citas.component';

import { PacientesComponent } from './pages/doctor/components/pacientes/pacientes.component';
import { AnotacionesMedicasComponent } from './pages/doctor/anotaciones-medicas/anotaciones-medicas.component';
import { InsEnfermeraComponent } from './pages/doctor/ins-enfermera/ins-enfermera.component';
import { ProgCirugiasComponent } from './pages/doctor/prog-cirugias/prog-cirugias.component';
import { SolicitaEstudiosComponent } from './pages/doctor/solicita-estudios/solicita-estudios.component';
import { HojaAnotacionesComponent } from './pages/doctor/anotaciones-medicas/hoja-anotaciones/hoja-anotaciones.component';
import { DashboardQuimicoComponent } from './pages/quimicos/dashboard-quimico/dashboard-quimico.component';

import { SolicitarEstudioComponent } from './pages/trabajador-social/solicitar-estudio/solicitar-estudio.component';
import { ConsultarEstudiosComponent } from './pages/trabajador-social/consultar-estudios/consultar-estudios.component';
import { ConsularEstudioQuimicoComponent } from './pages/quimicos/consular-estudio-quimico/consular-estudio-quimico.component';
import { PruebasPatologicasComponent } from './pages/quimicos/pruebas-patologicas/pruebas-patologicas.component';
import { DashboardContraloriaComponent } from './pages/contraloria/dashboard-contraloria/dashboard-contraloria.component';



import { RegistrarProvComponent } from './pages/contraloria/registrar-prov/registrar-prov.component';
import { ConsultarProvComponent } from './pages/contraloria/consultar-prov/consultar-prov.component';
import { CobroConsultasComponent } from './pages/contraloria/cobro-consultas/cobro-consultas.component';
import { CobroCirugiasComponent } from './pages/contraloria/cobro-cirugias/cobro-cirugias.component';
import { ControlGastosComponent } from './pages/contraloria/control-gastos/control-gastos.component';
import { DashboardInterconsultasComponent } from './pages/jefe-interconsultas/dashboard-interconsultas/dashboard-interconsultas.component';


import { RegistrarDoctorComponent } from './pages/trabajador-social/registrar-doctor/registrar-doctor.component';
import { ConsultarDoctoresComponent } from './pages/trabajador-social/consultar-doctores/consultar-doctores.component';
import { RegistrarEnfermeraComponent } from './pages/trabajador-social/registrar-enfermera/registrar-enfermera.component';

import { ConsultarEnfermeraComponent } from './pages/trabajador-social/consultar-enfermera/consultar-enfermera.component';
import { RegistrarPacienteComponent } from './pages/trabajador-social/registrar-paciente/registrar-paciente.component';
import { RegistrarGastosComponent } from './pages/contraloria/registrar-gastos/registrar-gastos.component';
import { RegistrarCirugiaComponent } from './pages/trabajador-social/registrar-cirugia/registrar-cirugia.component';
import { ConsultarCirugiaComponent } from './pages/trabajador-social/consultar-cirugia/consultar-cirugia.component';
import { EditarUsuariosComponent } from './pages/trabajador-social/editar-usuarios/editar-usuarios.component';

import { ConsultaPruebaComponent } from './pages/quimicos/consulta-prueba/consulta-prueba.component';

import { FiltroPipe } from './pipes/filtro.pipe';
import { ConsultarPacientesComponent } from './pages/trabajador-social/consultar-pacientes/consultar-pacientes.component';
import { AsesoriaHospitalariaComponent } from './pages/jefe-interconsultas/asesoria-hospitalaria/asesoria-hospitalaria.component';
import { SeguimientoPacientesComponent } from './pages/jefe-interconsultas/seguimiento-pacientes/seguimiento-pacientes.component';
import { InstruccionesDoctorComponent } from './pages/doctor/instrucciones-doctor/instrucciones-doctor.component';






@NgModule({
  declarations: [
    AppComponent,
    NopagesfoundComponent,
    LoginComponent,
    DashboardDoctorComponent,
    DashboardEnfermeraComponent,
    MisPacientesComponent,
    BanosPacientesComponent,
    CirugiasComponent,
    MaterialAsignadoComponent,
    SolicitarAseoComponent,
    TrabajadorSocialComponent,
    NuevoBanoComponent,
    InicioComponent,
    MiPerfilComponent,
    ControlCamasComponent,
    RegistrarCitasComponent,
    ConsultarCitasComponent,

    DashboardDoctorComponent,
    PacientesComponent,
    AnotacionesMedicasComponent,
    InsEnfermeraComponent,
    ProgCirugiasComponent,
    SolicitaEstudiosComponent,
    HojaAnotacionesComponent,
    DashboardQuimicoComponent,

    SolicitarEstudioComponent,
    ConsultarEstudiosComponent,
    ConsularEstudioQuimicoComponent,
    PruebasPatologicasComponent,
    DashboardContraloriaComponent,

    RegistrarProvComponent,
    ConsultarProvComponent,
    CobroConsultasComponent,
    CobroCirugiasComponent,
    ControlGastosComponent,


    RegistrarDoctorComponent,
    ConsultarDoctoresComponent,
    RegistrarEnfermeraComponent,

    ConsultarEnfermeraComponent,
    RegistrarPacienteComponent,



    DashboardInterconsultasComponent,
          RegistrarGastosComponent,
          RegistrarCirugiaComponent,
          ConsultarCirugiaComponent,
          EditarUsuariosComponent,

          ConsultaPruebaComponent,

          FiltroPipe,
          ConsultarPacientesComponent,
          AsesoriaHospitalariaComponent,
          SeguimientoPacientesComponent,
          InstruccionesDoctorComponent,

    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
