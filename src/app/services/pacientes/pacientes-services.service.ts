import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { PacientesModel } from 'src/app/models/trabajo-social/pacientes.model';

@Injectable({
  providedIn: 'root'
})
export class PacientesServicesService {

  url = "http://localhost:3000/api";

  constructor(private http:HttpClient) { }

  listarPacientes(){
    return this.http.get(this.url + '/listar-pacientes');
  }
  listarEnfermera(numero_cedula:string){
    return this.http.get(this.url + '/listarEnfermeraPacienteCED/'+numero_cedula);
  }
  listarPacienteGeneral(){
    return this.http.get(this.url + '/listarPacientesTrabajoSocial/');
  }
  listarPacientesCedula(cedula:string){
    return this.http.get(this.url + '/listarPacienteDoctorCargoTrabajoSocial/'+cedula);
  }

  registrarPaciente( modeloPacientes:PacientesModel ){
    return this.http.post( this.url + '/registrarPacienteTrabajoSocial', modeloPacientes );
  }
  eliminarPaciente(id:string){
    return this.http.delete( this.url + '/eliminarPacienteTrabajadorSocial/'+id );
  } 

}
