import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CirugiaModel } from 'src/app/models/doctor/cirugia.model';
import { Diagnostico } from 'src/app/models/doctor/diagnostico.model';

@Injectable({
  providedIn: 'root'
})
export class CirujiasService {

  url =  "http://localhost:3000/api";

  constructor(private http:HttpClient) { }

  listarCirujias(){
    return this.http.get(this.url+"/listar-cirujias");
  }

  registrarCirujias(modelo:CirugiaModel){
    return this.http.post(this.url+"/registrar-cirujias",modelo);

  }

  editarCirujias(id:any,modelo:CirugiaModel){
    return this.http.put(this.url+"/editar-cirujias/"+id,modelo);

  }

  /* listarCirujiasId(id:any){
    return this.http.get(this.url+"/listar-cirujiasDoctorEncargado"+id);

  } */

  eliminarCirujias(id:any){
    return this.http.delete(this.url+"/eliminar-cirujias/"+id);

  }

  /* =============================== */
  /* P A C I E N T E S  D O C T O R */
  /* ============================== */

  listarPacientes(){
    return this.http.get(this.url+'/listar-pacientes');
  }
  editarDiagnostico(id:any,diagnostico:Diagnostico){
    return this.http.put(this.url+'/editar-pacientes-diagnostico/'+id,diagnostico);
  }
}
