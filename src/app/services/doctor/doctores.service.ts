import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EstudioQuimicoModel } from 'src/app/models/quimicos/estudios-quimicos.model';

@Injectable({
  providedIn: 'root'
})
export class DoctoresService {

  url =  "http://localhost:3000/api";

  constructor( private http:HttpClient ) { }

  listarDoctoresRol(rolDoctor:string){
    return this.http.get( this.url + '/rol-listar/'+rolDoctor );
  }

  listarEnfermeras(){
    return this.http.get( this.url + '/' );
  }

  //Solicitar estudios quimicos
  registrarEstudiosQuimicosDoctorPacientes( modelo:EstudioQuimicoModel ){
    return this.http.post( this.url + '/registrar-estudio-quimico', modelo );
  }

}
