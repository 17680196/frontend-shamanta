import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IniciarSesion } from 'src/app/models/login/iniciar-sesion.model';
import { UsuariosModel } from 'src/app/models/login/registrar-usuarios.model';


@Injectable({
  providedIn: 'root'
})
export class IniciarSesionService {

  url = 'http://localhost:3000/api';
  constructor( private http: HttpClient ) { }

  login( modelo:IniciarSesion ){
    return this.http.post(this.url +'/login' ,modelo);
  }
  listarUsuariosEmail(email:string){
    return this.http.get(this.url+'/listar-user-email/'+email);
  }
  registrarUsuarios(modelo:UsuariosModel){
    return this.http.post(this.url+'/registrar-usuarios',modelo);
  }
  listarDoctores(){
    return this.http.get(this.url+'/rol-listar/'+"Doctor");
  }
  listarEnfermeros(){
    return this.http.get(this.url+"/rol-listar/"+"Enfermero");
  }
  //ListarUsuariosID
  listarUsuariosID(id:any){
    return this.http.get(this.url+'/listar-usuariosID/'+id);
  }
  //Borrar usuarios
  borrarUsuarios(id:any){
    return this.http.delete(this.url+"/eliminar-usuario/"+id);
  }

}
