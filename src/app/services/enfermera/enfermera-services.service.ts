import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnfermeraServicesService {

  url =  "http://localhost:3000/api";

  constructor(private http:HttpClient) { }

  //Listar pacientes según enfermera
  listarPacienteEnfermera(enfermeraCargo: string){
    return this.http.get(this.url+'/listar-pacienteEnfermera-idCama/'+enfermeraCargo);
  }

}
