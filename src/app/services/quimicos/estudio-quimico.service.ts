import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EstudioQuimicoModel } from 'src/app/models/quimicos/estudios-quimicos.model';

@Injectable({
  providedIn: 'root'
})
export class EstudioQuimicoService {
  url = 'http://localhost:3000/apiQuimico';

  constructor(private http:HttpClient) { }

  registrarSolicitudEstudio(modelo:EstudioQuimicoModel){
    return this.http.post(this.url + '/registrar-estudio-quimico',modelo);
  }
  listarEstudios(){
    return this.http.get(this.url + '/listar-estudios');
  }
}
