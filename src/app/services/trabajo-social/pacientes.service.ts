import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PacientesModel } from 'src/app/models/trabajo-social/pacientes.model';

@Injectable({
  providedIn: 'root'
})
export class PacientesService {

  url = "http://localhost:3000/api";

  constructor(private http: HttpClient) { }

  registrarPacientes(modelo:PacientesModel){
    return this.http.post(this.url+'/registrar-pacientes', modelo);
  }
  

}
