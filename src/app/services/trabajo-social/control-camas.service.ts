import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ControlCamasService {

  url =  "http://localhost:3000/api";

  constructor(private http:HttpClient) { }

  //Listar pacientes según enfermera
  listarCamas(){
      return this.http.get(this.url+'/listar-camas');
  }
  listarCamaDisponible(disponible:String){
    return this.http.get(this.url+'/listar-camaStatus/'+disponible);
  }

}
