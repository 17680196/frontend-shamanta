import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CitasMedicas } from 'src/app/models/trabajo-social/citas-medicas.model';

@Injectable({
  providedIn: 'root'
})
export class RegistrarCitaService {
  
  url = 'http://localhost:3000/api';

  constructor(private http:HttpClient) { }
/* Metodo 1 */
  registrarCita( modelo:CitasMedicas ){
    return this.http.post(this.url + '/registrar-cita', modelo);
  }
  /* Metodo 2 */
  listarCitas():Observable<any>{
    return this.http.get(this.url+'/listar-citas');
  }
  /* Metodo 3 */
  listarDoctores(rol:string){
    return this.http.get(this.url+'/listarUser-rol/'+rol);
  }

}
