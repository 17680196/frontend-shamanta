export class CitasMedicas{

    nombre_solicitante:string;
    fecha:string;
    hora:string;
    asunto:string;
    nombre_doctor:string;
    numero_consultorio:string;

    constructor( nombre_solicitante:string, fecha:string, hora:string, asunto:string, nombre_doctor:string, numero_consultorio:string ){
        this.nombre_solicitante = nombre_solicitante;
        this.fecha = fecha;
        this.hora = hora;
        this.asunto = asunto;
        this.nombre_doctor = nombre_doctor;
        this.numero_consultorio = numero_consultorio;
    }

}