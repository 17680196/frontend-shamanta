export class PacientesModel{
    idCama:string;
    nombre:string;
    apellido_paterno:string;
    apellido_materno:string;
    peso:string;
    edad:string;
    diagnostico:string;
    tipoSangre:string;
    doctorEncargado:string;
    enfermeraCargo: string;

    constructor(idCama:string, nombre:string, apellido_paterno:string, apellido_materno:string, peso:string,edad:string, diagnostico:string, tipoSangre:string, doctorEncargado:string, enfermeraCargo:string){
        this.idCama = idCama;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.peso = peso;
        this.edad = edad;
        this.diagnostico = diagnostico; 
        this.tipoSangre = tipoSangre;
        this.doctorEncargado = doctorEncargado;
        this.enfermeraCargo = enfermeraCargo;
    }

}