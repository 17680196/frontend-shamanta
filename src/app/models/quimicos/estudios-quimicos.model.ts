export class EstudioQuimicoModel{
    nombre_paciente:string;
    numero_habitacion:string;
    estudio_solicitado:string;
    fecha:string;
    hora:string;
    nombre_doctor:string;
    nivel_prioridad:string;

    constructor(nombre_paciente:string, numero_habitacion:string,estudio_solicitado:string,fecha:string,hora:string,nombre_doctor:string,nivel_prioridad:string){
        this.nombre_paciente = nombre_paciente;
        this.numero_habitacion = numero_habitacion;
        this.estudio_solicitado = estudio_solicitado;
        this.fecha = fecha;
        this.hora = hora;
        this.nombre_doctor = nombre_doctor;
        this.nivel_prioridad = nivel_prioridad;
    }

}