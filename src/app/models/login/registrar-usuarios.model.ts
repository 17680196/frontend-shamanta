export class UsuariosModel{
    email:string;
    password:string;
    nombre:string;
    apellido_paterno:string;
    apellido_materno:string;
    rol:string;
    numero_cedula:string;
    celular:string;
    sexo:string;


    constructor( email:string,password:string,nombre:string,apellido_paterno:string, apellido_materno:string,rol:string,numero_cedula:string,celular:string,sexo:string){
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.rol = rol;
        this.numero_cedula = numero_cedula;
        this.celular = celular;
        this.sexo = sexo;
    }

}