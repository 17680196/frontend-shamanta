export class CirugiaModel{

    idConsultorio: string;
    nombre: string;
    tipo_cirugia: string;
    fecha: string;
    hora: string;
    nombre_doctor: string;

    constructor(idConsultorio: string,
        nombre: string,
        tipo_cirugia: string,
        fecha: string,
        hora: string,
        nombre_doctor: string){

            this.idConsultorio =idConsultorio;
            this.nombre = nombre;
            this.tipo_cirugia = tipo_cirugia;
            this.fecha = fecha;
            this.hora = hora;
            this.nombre_doctor = nombre_doctor;
        }

}
